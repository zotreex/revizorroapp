package com.zotreex.revizorro.di

import com.zotreex.revizorro.presentor.MainFragment
import dagger.Component

@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(fragment: MainFragment)
}
