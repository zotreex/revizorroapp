package com.zotreex.revizorro

import android.app.Application
import android.content.Context
import com.zotreex.revizorro.di.AppComponent
import com.zotreex.revizorro.di.AppModule
import com.zotreex.revizorro.di.DaggerAppComponent

class MainApplication : Application() {
    lateinit var appComponent: AppComponent
        private set

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }
}

val Context.appComponent: AppComponent
    get() = when (this) {
        is MainApplication -> appComponent
        else -> applicationContext.appComponent
    }

