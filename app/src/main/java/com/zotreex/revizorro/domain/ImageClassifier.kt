package com.zotreex.revizorro.domain

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import com.zotreex.revizorro.R
import com.zotreex.revizorro.data.ClassifiedImage
import com.zotreex.revizorro.data.ClassifiedType
import com.zotreex.revizorro.ml.ImageClassificator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import org.tensorflow.lite.support.image.TensorImage
import javax.inject.Inject

class ImageClassifier @Inject constructor(
    private val context: Context,
    private val settingsRepository: SettingsRepository
) {
    operator fun invoke(bitmap: Bitmap): Flow<ClassifiedImage> = flow {
        val model = ImageClassificator.newInstance(context)
        val image = TensorImage.fromBitmap(bitmap)
        val outputs = model.process(image)
        val probability = outputs.probabilityAsCategoryList

        val winner = probability.apply {
            sortByDescending { it.score }
        }.first()
        model.close()

        val type =
            if (winner.score >= settingsRepository.imageLimitScore)
                ClassifiedType.from(winner.label)
            else
                ClassifiedType.UNSUPPORTED

        emit(
            ClassifiedImage(
                score = winner.score,
                type = type
            )
        )
    }.flowOn(Dispatchers.IO)
}