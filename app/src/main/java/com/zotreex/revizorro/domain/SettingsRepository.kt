package com.zotreex.revizorro.domain

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import javax.inject.Inject

class SettingsRepository @Inject constructor(
    private val context: Context
) {
    private var prefs: SharedPreferences =
        context.getSharedPreferences("Settings", Context.MODE_PRIVATE)

    var imageLimitScore
        get() = prefs.getFloat(IMAGE_LIMIT_SCORE_PREF, 0.5f)
        set(value) {
            prefs.edit {
                putFloat(IMAGE_LIMIT_SCORE_PREF, value)
                apply()
            }
        }

    var objectLimitScore
        get() = prefs.getFloat(OBJECT_LIMIT_SCORE_PREF, 0.5f)
        set(value) {
            prefs.edit {
                putFloat(OBJECT_LIMIT_SCORE_PREF, value)
                apply()
            }
        }

    companion object {
        private const val IMAGE_LIMIT_SCORE_PREF = "imageLimitScore"
        private const val OBJECT_LIMIT_SCORE_PREF = "objectLimitScore"
    }
}