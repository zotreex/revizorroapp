package com.zotreex.revizorro.domain

import android.content.Context
import android.graphics.Bitmap
import com.zotreex.revizorro.data.DetectedObject
import com.zotreex.revizorro.data.DetectedObjectType
import com.zotreex.revizorro.ml.Model
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import org.tensorflow.lite.support.image.TensorImage
import javax.inject.Inject

class ObjectDetector @Inject constructor(
    private val context: Context,
    private val settingsRepository: SettingsRepository
) {
    operator fun invoke(bitmap: Bitmap): Flow<List<DetectedObject>> = flow {
        val image = TensorImage.fromBitmap(bitmap)
        val model = Model.newInstance(context)
        val outputs = model.process(image)

        val result = mutableListOf<DetectedObject>()

        outputs.detectionResultList.forEach {
            if (it.scoreAsFloat >= settingsRepository.objectLimitScore)
                result.add(
                    DetectedObject(
                        score = it.scoreAsFloat,
                        location = it.locationAsRectF,
                        type = DetectedObjectType.from(it.categoryAsString)
                    )
                )
        }

        emit(result)
    }.flowOn(Dispatchers.IO)
}