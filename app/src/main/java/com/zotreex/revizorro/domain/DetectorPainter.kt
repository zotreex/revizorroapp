package com.zotreex.revizorro.domain

import android.graphics.*
import com.zotreex.revizorro.data.DetectedObjectType
import com.zotreex.revizorro.data.DrawItem
import javax.inject.Inject

class DetectorPainter @Inject constructor() {

    private var tasksList = listOf<DrawItem>()

    fun setTasks(tasks: List<DrawItem>): DetectorPainter {
        tasksList = tasks
        return this
    }

    fun draw(bitmap: Bitmap): Bitmap {
        var resultBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true)
        var canvas = Canvas(resultBitmap)
        tasksList.forEach {
            canvas = drawOnCanvas(canvas, it.location, it.objectType, it.imageType.name)
        }
        return resultBitmap
    }

    private fun drawOnCanvas(
        canvas: Canvas,
        location: RectF,
        type: DetectedObjectType,
        name: String
    ): Canvas {

        val categoryColor = when (type) {
            DetectedObjectType.ITEM -> Color.BLUE
            DetectedObjectType.PRICE -> Color.RED
            else -> Color.YELLOW
        }

        Paint().apply {
            color = categoryColor
            isAntiAlias = true
            style = Paint.Style.STROKE
            strokeWidth = 10F

            // draw rectangle on canvas
            canvas.drawRect(
                location.left, // left side of the rectangle to be drawn
                location.top, // top side
                location.right, // right side
                location.bottom, // bottom side
                this
            )
        }

        if (type == DetectedObjectType.ITEM) {
            val rect = Rect()
            Paint().apply {
                color = categoryColor
                style = Paint.Style.FILL_AND_STROKE
                strokeWidth = 10F
                textSize = 40f
                getTextBounds(name, 0, name.length, rect)
                canvas.drawRect(
                    location.left, // left side of the rectangle to be drawn
                    location.top + rect.top.toFloat(), // top side
                    location.left + rect.right.toFloat(), // right side
                    location.top, // bottom side
                    this
                )

            }
            Paint().apply {
                textSize = 40f
                color = Color.WHITE
                canvas.drawText(name, location.left, location.top, this)
            }
        }

        return canvas
    }
}