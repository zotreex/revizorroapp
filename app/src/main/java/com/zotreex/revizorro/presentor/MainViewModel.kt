package com.zotreex.revizorro.presentor

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.zotreex.revizorro.data.DetectedObjectType
import com.zotreex.revizorro.data.DrawItem
import com.zotreex.revizorro.data.UiState
import com.zotreex.revizorro.domain.DetectorPainter
import com.zotreex.revizorro.domain.ImageClassifier
import com.zotreex.revizorro.domain.ObjectDetector
import com.zotreex.revizorro.helpers.cropByRectF
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val objectDetector: ObjectDetector,
    private val imageClassifier: ImageClassifier,
    private val detectorPainter: DetectorPainter
) : ViewModel() {
    val detectionState = MutableLiveData<UiState<Bitmap>>(UiState.Start)

    fun detekt(bitmap: Bitmap) {
        detectionState.postValue(UiState.Loading)
        viewModelScope.launch {
            var tasks = mutableListOf<DrawItem>()
            objectDetector(bitmap).collect { detectedObjects ->
                detectedObjects.forEach { detectedObject ->
                    if (detectedObject.type == DetectedObjectType.ITEM)
                        imageClassifier(bitmap.cropByRectF(detectedObject.location)).collect { image ->
                            tasks.add(
                                DrawItem(
                                    image.score,
                                    detectedObject.location,
                                    detectedObject.type,
                                    image.type
                                )
                            )
                        }
                    else {
                        tasks.add(
                            DrawItem(
                                detectedObject.score,
                                detectedObject.location,
                                detectedObject.type
                            )
                        )
                    }
                }
            }
            detectionState.postValue(UiState.Success(detectorPainter.setTasks(tasks).draw(bitmap)))

        }
    }
}