package com.zotreex.revizorro.presentor

import android.content.Context
import android.graphics.*
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.zotreex.revizorro.BuildConfig
import com.zotreex.revizorro.R
import com.zotreex.revizorro.appComponent
import com.zotreex.revizorro.data.UiState
import com.zotreex.revizorro.databinding.FragmentMainBinding
import com.zotreex.revizorro.di.ViewModelFactory
import java.io.File
import javax.inject.Inject


class MainFragment : Fragment(R.layout.fragment_main) {
    @Inject
    lateinit var factory: ViewModelFactory

    private val binding: FragmentMainBinding by viewBinding()
    private val viewModel: MainViewModel by viewModels<MainViewModel> { factory }

    private var tmpUri: Uri? = null
    private val takeImageResult =
        registerForActivityResult(ActivityResultContracts.TakePicture()) { isSuccess ->
            if (isSuccess) {
                binding.photoContainer.setImageURI(tmpUri)
                tmpUri?.let { startDetekt(it) }
            }
        }

    private val selectImageFromGalleryResult =
        registerForActivityResult(ActivityResultContracts.GetContent()) {
            binding.photoContainer.setImageURI(it)
            it?.let { startDetekt(it) }
        }

    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                lifecycleScope.launchWhenStarted {
                    getTmpFileUri().let {
                        tmpUri = it
                        takeImageResult.launch(it)
                    }
                }
            } else {
                Log.e("Issue", "Failed Permission")
            }
        }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        context.appComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.takePhoto.setOnClickListener {
            onTakePhotoClick()
        }

        binding.selectPhoto.setOnClickListener {
            selectImageFromGalleryResult.launch("image/*")
        }

        binding.realTime.setOnClickListener {
            MainFragmentDirections.toRealTime().also {
                findNavController().navigate(it)
            }
        }

        viewModel.detectionState.observe(viewLifecycleOwner) {
            it ?: return@observe

            when(it) {
                is UiState.Start -> {
                    binding.welcomeText.visibility = View.VISIBLE
                }
                is UiState.Success -> {
                    binding.photoContainer.setImageBitmap(it.data)
                    binding.progress.visibility = View.GONE
                }
                is UiState.Loading -> {
                    binding.welcomeText.visibility = View.GONE
                    binding.progress.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun onTakePhotoClick() {
        requestPermissionLauncher.launch(android.Manifest.permission.CAMERA)
    }

    private fun getTmpFileUri(): Uri {
        val tmpFile = File.createTempFile("tmp_image_file", ".png").apply {
            createNewFile()
            deleteOnExit()
        }

        return FileProvider.getUriForFile(
            requireContext(),
            "${BuildConfig.APPLICATION_ID}.provider",
            tmpFile
        )
    }

    private fun startDetekt(uri: Uri) {
        var bitmap = ImageDecoder.decodeBitmap(
            ImageDecoder.createSource(
                requireContext().contentResolver,
                uri
            )
        )
        var image = bitmap.copy(Bitmap.Config.ARGB_8888,true)
        viewModel.detekt(image)
    }
}