package com.zotreex.revizorro.helpers

import android.graphics.Bitmap
import android.graphics.RectF
import kotlin.math.min

fun Bitmap.cropByRectF(rectF: RectF): Bitmap = Bitmap.createBitmap(
    this,
    if(rectF.left.toInt() < 0) 0 else rectF.left.toInt(),
    if(rectF.top.toInt() < 0) 0 else rectF.top.toInt(),
    min((rectF.right - rectF.left).toInt(), width - rectF.left.toInt()),
    min((rectF.bottom - rectF.top).toInt(), height - rectF.top.toInt()),
    null,
    false
)