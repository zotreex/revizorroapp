package com.zotreex.revizorro.helpers

import android.util.Log

fun log(str: String) {
    Log.e("Log_tag", str)
}