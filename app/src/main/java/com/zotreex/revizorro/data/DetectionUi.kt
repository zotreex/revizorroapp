package com.zotreex.revizorro.data

import android.graphics.Bitmap

data class DetectionUi(
    var bitmap: Bitmap
)