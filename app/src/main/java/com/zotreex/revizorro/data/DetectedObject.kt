package com.zotreex.revizorro.data

import android.graphics.RectF

data class DetectedObject(val score: Float, val location: RectF, val type: DetectedObjectType)

enum class DetectedObjectType(val rawName: String) {
    ITEM("item"), PRICE("price"), PRICE_TAG("price tag"), DEFAULT("default");

    companion object {
        fun from(type: String): DetectedObjectType = values().find { it.rawName == type } ?: DEFAULT
    }
}