package com.zotreex.revizorro.data

import android.graphics.RectF

data class DrawItem(
    val score: Float,
    val location: RectF,
    val objectType: DetectedObjectType,
    var imageType: ClassifiedType = ClassifiedType.SKIP
)
