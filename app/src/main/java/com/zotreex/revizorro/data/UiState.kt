package com.zotreex.revizorro.data

sealed class UiState<out T : Any> {
    object Start : UiState<Nothing>()

    object Loading : UiState<Nothing>()

    class Success<out T : Any>(val data: T) : UiState<T>()

    class Error<out T : Any>(val error: String) : UiState<T>()
}