package com.zotreex.revizorro.data

data class ClassifiedImage(
    var type: ClassifiedType = ClassifiedType.UNSUPPORTED,
    var score: Float,
)

enum class ClassifiedType(val rowName: String) {
    SKIP("SKIP"),
    UNSUPPORTED("UNSUPPORTED"),
    CHOCOPIE("chocopie"),
    COINTREAU("cointreau"),
    ERMIGURT("ermigurt"),
    FRUTONYANYA("frutonyanya"),
    GRANATE_JUICE("granate juice"),
    GRECHA("grecha"),
    HENNESSY("hennessy"),
    ICE_CREAM("icecream"),
    PERVAK("pervak"),
    VODKA("vodka");

    companion object {
        fun from(type: String): ClassifiedType =
            ClassifiedType.values().find { it.rowName == type } ?: UNSUPPORTED
    }
}